/*Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

Асинхронность програмування дає можливість виконувати код у порядку, який задано розробником і уникати помилок які виникають при тривалій загрузки файлів, т.є не виконується наступний код поки не підгрузиться вся необхідна інформация для його виконання.*/

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.


const button = document.getElementById('button');

button.addEventListener('click', event => {

    async function getIpAddress() {

        const IpAddress = 'https://api.ipify.org/?format=json';

        const response = await fetch(IpAddress)
        const userIp = await response.json()
        return userIp
    }
    
    async function getLocationAddress (){
        
        const userAddress = await getIpAddress();
        console.log(userAddress.ip)

        const getUserLocationAddress = `http://ip-api.com/json/${userAddress.ip}?fields=continent,country,regionName,city,district`;

        const response = await fetch(getUserLocationAddress)
        const LocationAddress = await response.json()
        return LocationAddress
       
    }

    async function dataUserLocationAddress (){

        const userLocationAddress = await getLocationAddress()

        const userContinent = document.createElement('h2');
        userContinent.textContent = userLocationAddress.continent;
        const userCountry = document.createElement('p')
        userCountry.textContent = userLocationAddress.country;
        const userRegionName = document.createElement('p')
        userRegionName.textContent = userLocationAddress.regionName;
        const userCity = document.createElement('p')
        userCity.textContent = userLocationAddress.city;
        const userDistrict = document.createElement('p')
        userDistrict.textContent = userLocationAddress.district

        button.insertAdjacentElement('afterEnd', userContinent)
        userContinent.insertAdjacentElement('afterEnd', userCountry)
        userCountry.insertAdjacentElement('afterEnd', userRegionName)
        userRegionName.insertAdjacentElement('afterEnd', userCity)
        userCity.insertAdjacentElement('afterEnd', userDistrict)
        
    }

    dataUserLocationAddress ()
    
})





